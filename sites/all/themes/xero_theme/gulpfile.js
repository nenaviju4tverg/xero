var gulp            = require('gulp'),
    autoprefixer    = require('gulp-autoprefixer'),
    svgmin          = require('gulp-svgmin'),
    imagemin        = require('gulp-imagemin'),
    watch           = require('gulp-watch'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    pngquant        = require('imagemin-pngquant');

var path = {
    build: {
        css:    'css/',
        img:    'img/'
    },
    src: {
        sass: 'scss/**/*.scss',
        img: 'src/img/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
      sass: 'scss/**/*.scss',
      img: 'src/img/**/*.*'
    }
};



// SASS-BUILD
gulp.task('sass:build', function() {
    var supportedBrowsers = [
        '> 0.5%',
        'last 2 versions',
        'ie >= 10',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.1',
        'bb >= 10'
    ];
    gulp.src(path.src.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: supportedBrowsers,
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
});

// IMAGE-BUILD
gulp.task('images:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            pngquant: true,
            svgoPlugins: [{removeViewBox: false}],
            use:[pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});

// GLUE ALL BUILDS
gulp.task('build', [
    'sass:build',
    'images:build'
]);

// WATCH-TASK
gulp.task('watch', function(){
  watch([path.watch.sass], function(event, cb) {
    gulp.start('sass:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('images:build');
  });
});

gulp.task('default', ['build', 'watch']);
