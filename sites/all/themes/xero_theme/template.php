<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function xero_theme_preprocess_page(&$vars) {
	if($vars['is_front']) {
          $vars['title'] = '';    
    }
    unset($vars['page']['content']['system_main']['default_message']);
}

function xero_theme_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'webform_client_form_2') {
        foreach ($form["submitted"] as $key => $value) {
            if (is_array($value)){
                $form["submitted"][$key]['#attributes']["placeholder"] = t($value["#title"]);
                $form["submitted"][$key]['#attributes']["onfocus"] = "this.placeholder = ''";
                $form["submitted"][$key]['#attributes']["onblur"] = "this.placeholder =" . "'" . t($value["#title"]) . "'";
                $form["submitted"][$key]['#title_display'] = 'invisible';
            }
        }
    }
}