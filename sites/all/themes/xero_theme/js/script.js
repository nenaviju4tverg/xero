/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {

      // Place your code here.

    }
  };

  $(window).ready(function () {
    $(window).resize(function () {
      var width1 = $('body').innerWidth();
      if (width1 < 1201) {
        $('.packages-block .view-content').addClass('carousel slide');
        $('.block-menu-head-menu').addClass('collapse');
      }
      else {
        $('.packages-block .view-content').removeClass('carousel slide');
        $('.block-menu-head-menu').removeClass('collapse');
      }
    });
  });

  $('.packages-block .view-content').carousel({
    interval: 2000
  })

})(jQuery, Drupal, this, this.document);
