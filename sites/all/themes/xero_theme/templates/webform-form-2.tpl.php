<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 23.04.2017
 * Time: 12:39
 */
$class_text_wrap = drupal_is_front_page() ? "col-xs-12 col-lg-7 col-sm-6" : "";
$class_field_wrap = drupal_is_front_page() ? "col-xs-12 col-lg-5 col-sm-6" : "";
$class_container_wrapper_wrap = drupal_is_front_page() ? "container" : "";
$class_row_wrapper_wrap = drupal_is_front_page() ? "container" : "";

?>
<div class="<?php print $class_container_wrapper_wrap; ?>">
    <div class="<?php print $class_row_wrapper_wrap; ?>">
        <div class="text-form-wrapper <?php print $class_text_wrap; ?>">
            <?php print drupal_render($form['submitted']['text_form_request']);?>
        </div>

        <div class="field-form-wrapper <?php print $class_field_wrap; ?>">
            <?php print drupal_render($form['submitted']['first_name']);?>
            <?php $form['submitted']['field_name']['#attributes']['placeholder'] = t('Text'); ?>
            <?php print drupal_render($form['submitted']['last_name']);?>
            <?php print drupal_render($form['submitted']['email']);?>
            <?php print drupal_render($form['submitted']['phone']);?>
            <?php print drupal_render($form['submitted']['message']);?>
            <?php print drupal_render_children($form);?>
        </div>
    </div>
</div>
